# Description

HW done

Create a new instance:

- Use separated files

- Create 2 disks — hdd and ssd

- Create a VPC and use an elastic IP

- Use the following parameters:

1. Instance name

2. Instance type

3. Whitelist

4. Tags = (dev, qa, stg)

- Gitignore (.terraform and terraform.tfstate files)

## Initial configuration. 
 Since each project has specific environment variables we want you to configure .tfvars file