resource "aws_vpc" "new-hw" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_hostnames = true
}

resource "aws_security_group" "hw" {
  name        = "vpc-sg-hw"
  vpc_id      = aws_vpc.new-hw.id
  description = "whitelist SG"
  lifecycle {
    create_before_destroy = true
  }
  # allow traffic for TCP 80
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # allow traffic from  TCP 80
  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = var.witelist
  }
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.13/32"]
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.new-hw.id
}

resource "aws_subnet" "tf_test_subnet" {
  vpc_id                  = aws_vpc.new-hw.id
  cidr_block              = "10.0.0.0/24"
  map_public_ip_on_launch = true
  depends_on              = [aws_internet_gateway.gw]
}

resource "aws_route" "internet" {
  route_table_id         = aws_vpc.new-hw.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gw.id
}

resource "aws_eip" "elip_front" {
  vpc        = true
  instance   = aws_instance.hw-front.id
  depends_on = [aws_internet_gateway.gw]
}