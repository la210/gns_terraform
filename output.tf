output "connection_front_string" {
  value = "ssh -i ~/.ssh/hw-key ubuntu@${aws_eip.elip_front.public_ip}"
}

output "connection_db_string" {
  value = "ssh -i ~/.ssh/hw-key ubuntu@${aws_instance.hw-db.public_ip}"
}