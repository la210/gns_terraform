/*------------------- Virtual machine ------------------------*/
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "hw-db" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.vm_config_db.vm_size
  private_ip             = "10.0.0.12"
  subnet_id              = aws_subnet.tf_test_subnet.id
  key_name               = aws_key_pair.hw-key.key_name
  vpc_security_group_ids = aws_security_group.hw.*.id


  tags = {
    Name = var.vm_config_db.name
    Env  = var.vm_config_db.env_tag
  }
}

resource "aws_instance" "hw-front" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.vm_config_front.vm_size
  private_ip             = "10.0.0.13"
  subnet_id              = aws_subnet.tf_test_subnet.id
  key_name               = aws_key_pair.hw-key.key_name
  vpc_security_group_ids = aws_security_group.hw.*.id


  tags = {
    Name = var.vm_config_front.name
    Env  = var.vm_config_front.env_tag
  }
}
